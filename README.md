# README #
Create a REST api with typescript and the framework of your choice (you can use expressjs) to manage social media posts (like twitter posts) for some user. The api should use data from a sqlite database

* setup your local environement, put requirements in the package.json and build a docker environement with a basic node server image and the sqlite database, the server expose the PORT 3000

* use sequelize  to manage models and database, it can be stored in local file, you can use uuid to generate unique Ids, we have  2 tables :

	* User with id, firstname, lastname, and unique email

	* Post with id, text, status (published, hidden, deleted), owner, creation date, modification date

* add a health endpoint to check if the server and database are healthy

* create CRUD operations for User, we should have validation on data for creation where all data is required

* create CRUD Operations for Post, in the url, user id is required, and the default status is published, we cannot change the owner, and each time the post is edited, the modification date has to be updtaed

* add authentication key in the header for all create update and delete operations

* the api should never return a 500 error, so take attention to catch exeptions and return a message

* if there is any validation error return a bad request code with a clear message

Good luck :) 